﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment1a_N01297520
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<string> spclfaci = new List<string> { };
            string stat = Utilities.ToString();

            string name = clientName.Text.ToString();
            string email = clientEmail.Text.ToString();
            string phone = clientPhone.Text.ToString();
            string age = clientAge.Text.ToString();
            Customer newcustomer = new Customer();
            newcustomer.CustomerFullname = name;
            newcustomer.CustomerEmailaddress = email;
            newcustomer.CustomerPhone = phone;
            newcustomer.CustomerAge = age;

            List<string> reservation = new List<string>();
            foreach(Control control in membership.Controls)
            {
                if(control.GetType()==typeof(CheckBox))
                {
                    CheckBox gym = (CheckBox)control;
                    if(gym.Checked)
                    {
                        membership.Add(gym.Text);   //from inclass assignment
                        
                    } 
                }
            }

        }
    }
}