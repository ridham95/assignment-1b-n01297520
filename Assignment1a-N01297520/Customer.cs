﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment1a_N01297520
{
    public class Customer
    {
        private string clientFullname;
        private string clientEmailaddress;
        private string clientPhone;
        private string clientAge;

        public Customer()
        {

        }

        public string CustomerFullname
        {
            get { return CustomerFullname; }
            set { CustomerFullname = value; }
        }
        public string CustomerEmailaddress
        {
            get { return CustomerEmailaddress; }
            set { CustomerEmailaddress = value; }
        }
        public string CustomerPhone
        {
            get { return CustomerPhone; }
            set { CustomerPhone = value; }
        }
        public string CustomerAge
        {
            get { return CustomerAge; }
            set { CustomerAge = value; }
        }
    }
}