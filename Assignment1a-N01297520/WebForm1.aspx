﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="Assignment1a_N01297520.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <p>Gym Reservation.</p>
            <asp:TextBox runat="server" ID="clientName" placeholder="Full Name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your Name" ControlToValidate="clientName" ID="validatorName"></asp:RequiredFieldValidator>
            <br />
            <br />
            <asp:TextBox runat="server" ID="clientEmail" placeholder="Email Address"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your Email Address" ControlToValidate="clientEmail" ID="RequiredFieldValidator1"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="clientEmail" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator>
            <br />
            <br />
            <asp:TextBox runat="server" ID="clientPhone" placeholder="Phone"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a valid Phone Number" ControlToValidate="clientPhone" ID="validatorPhone"></asp:RequiredFieldValidator>
            <br /><br />
             <asp:TextBox runat="server" ID="clientAge" placeholder="Age"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your Age" ControlToValidate="clientAge" ID="RequiredFieldValidator2"></asp:RequiredFieldValidator>
            <br />
            <br />
             <asp:RadioButton runat="server" Text="Male" GroupName="via"/>
            <asp:RadioButton runat="server" Text="Female" GroupName="via"/>
            <br />
            <br />
            <asp:DropDownList runat="server" ID="membership" placeholder="Type of Membership">
                <asp:ListItem Value="SR" Text="Monthly Package"></asp:ListItem>
                <asp:ListItem Value="LR" Text="Yearly Package"></asp:ListItem>
                <asp:ListItem Value="LS" Text="Silver Package"></asp:ListItem>
                <asp:ListItem Value="SK" Text="Gold Package"></asp:ListItem>
                <asp:ListItem Value="RV" Text="Platinum Package"></asp:ListItem>
            </asp:DropDownList>
            <br />
            </br>
            <div id="Utilities" runat="server">
            <asp:CheckBox runat="server" ID="utilities1" Text="Aerobics" /> </br>
            <asp:CheckBox runat="server" ID="utilities2" Text="Salsa" /> </br>
            <asp:CheckBox runat="server" ID="utilities3" Text="Spa" /> </br>
            <asp:CheckBox runat="server" ID="CheckBox1" Text="Gym" /> </br>
            <asp:CheckBox runat="server" ID="CheckBox2" Text="Gym+Spa" /> </br>
            </div>
            <br />
            <asp:RadioButton runat="server" Text="Cash" GroupName="via"/>
            <asp:RadioButton runat="server" Text="Debit/Credit" GroupName="via"/>
            <br />
            <br />
            <asp:Button runat="server" ID="myButton"  Text="CONFIRM"/>
            <br />
            <div runat="server" ID="res"></div>
            <br />
        </div>
    </form>
</body>
</html>
